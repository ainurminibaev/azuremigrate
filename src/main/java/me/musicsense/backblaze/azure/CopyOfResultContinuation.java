package me.musicsense.backblaze.azure;

import java.io.Serializable;

import com.microsoft.azure.storage.ResultContinuationType;
import com.microsoft.azure.storage.StorageLocation;

/**
 * Created by ainurminibaev on 29.06.16.
 */
public class CopyOfResultContinuation implements Serializable {
    private String nextMarker;

    /**
     * Represents the next partition key for TableServiceEntity enumeration operations.
     */
    private String nextPartitionKey;

    /**
     * Represents the next row key for TableServiceEntity enumeration operations.
     */
    private String nextRowKey;

    /**
     * Represents the next table name for Table enumeration operations.
     */
    private String nextTableName;

    /**
     * Represents the type of the continuation token.
     */
    private ResultContinuationType continuationType;

    /**
     * Represents the location that the token applies to.
     */
    private StorageLocation targetLocation;

    public String getNextMarker() {
        return nextMarker;
    }

    public void setNextMarker(String nextMarker) {
        this.nextMarker = nextMarker;
    }

    public String getNextPartitionKey() {
        return nextPartitionKey;
    }

    public void setNextPartitionKey(String nextPartitionKey) {
        this.nextPartitionKey = nextPartitionKey;
    }

    public String getNextRowKey() {
        return nextRowKey;
    }

    public void setNextRowKey(String nextRowKey) {
        this.nextRowKey = nextRowKey;
    }

    public String getNextTableName() {
        return nextTableName;
    }

    public void setNextTableName(String nextTableName) {
        this.nextTableName = nextTableName;
    }

    public ResultContinuationType getContinuationType() {
        return continuationType;
    }

    public void setContinuationType(ResultContinuationType continuationType) {
        this.continuationType = continuationType;
    }

    public StorageLocation getTargetLocation() {
        return targetLocation;
    }

    public void setTargetLocation(StorageLocation targetLocation) {
        this.targetLocation = targetLocation;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CopyOfResultContinuation that = (CopyOfResultContinuation) o;

        if (nextMarker != null ? !nextMarker.equals(that.nextMarker) : that.nextMarker != null) return false;
        if (nextPartitionKey != null ? !nextPartitionKey.equals(that.nextPartitionKey) : that.nextPartitionKey != null) return false;
        if (nextRowKey != null ? !nextRowKey.equals(that.nextRowKey) : that.nextRowKey != null) return false;
        if (nextTableName != null ? !nextTableName.equals(that.nextTableName) : that.nextTableName != null) return false;
        if (continuationType != that.continuationType) return false;
        return targetLocation == that.targetLocation;

    }

    @Override
    public int hashCode() {
        int result = nextMarker != null ? nextMarker.hashCode() : 0;
        result = 31 * result + (nextPartitionKey != null ? nextPartitionKey.hashCode() : 0);
        result = 31 * result + (nextRowKey != null ? nextRowKey.hashCode() : 0);
        result = 31 * result + (nextTableName != null ? nextTableName.hashCode() : 0);
        result = 31 * result + (continuationType != null ? continuationType.hashCode() : 0);
        result = 31 * result + (targetLocation != null ? targetLocation.hashCode() : 0);
        return result;
    }
}
