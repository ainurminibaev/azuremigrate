package me.musicsense.backblaze.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.StorageObject;
import me.musicsense.backblaze.google.util.CredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.net.HttpURLConnection.HTTP_CONFLICT;

/**
 * Created by ainurminibaev on 29.06.16.
 */
public class GoogleStorageUploadTool {

    private final static Logger logger = LoggerFactory.getLogger(GoogleStorageUploadTool.class);
    private static final String BUCKET_LOCATION = "europe-west1";
    public static final String PROJECT_NAME = "musicsense-test";
    private static final int MAX_RETRY_COUNT = 3;
    private final String containerName;
    private final Storage storage;


    public GoogleStorageUploadTool(String containerName, String jsonCreds) {
        this.containerName = containerName;
        HttpTransport httpTransport = null;
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            Credential credential = null;
            credential = CredentialsProvider.authorize(jsonCreds);
            storage = new Storage.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName("MusicSenseBucket/1.0").build();
            Bucket bucket = get(storage, containerName);
            if (bucket == null) {
                bucket = createInProject(storage, PROJECT_NAME);
            }
        } catch (Exception e) {
            logger.debug("Error on google load", e);
            throw new RuntimeException(e);
        }
    }

    private Bucket get(Storage storage, String bucketName) throws IOException {
        Storage.Buckets.Get getBucket = storage.buckets().get(bucketName);
        getBucket.setProjection("full");  // if you are interested in acls.
        try {
            return getBucket.execute();
        } catch (GoogleJsonResponseException e) {
            if (e.getDetails().getCode() == 404) {
                return null;
            }
        }
        return null;
    }

    private Bucket createInProject(Storage storage, String project)
            throws IOException {
        Bucket bucket = new Bucket().setName(containerName)
                .setLocation(BUCKET_LOCATION);
//        BucketAccessControl bucketAccessControl = new BucketAccessControl();
//        bucketAccessControl.setKind("public-read");
//        bucketAccessControl.setDomain("gs://bucket");
//        List<BucketAccessControl> bucketAccessControls = Collections.singletonList(bucketAccessControl);
//        bucket.setAcl(bucketAccessControls);
        try {
            Storage.Buckets.Insert insertBucket = storage.buckets().insert(project, bucket);
            return insertBucket.execute();
        } catch (GoogleJsonResponseException e) {
            GoogleJsonError error = e.getDetails();
            if (error != null && error.getCode() == HTTP_CONFLICT && error.getMessage().contains("You already own this bucket.")) {
                System.out.println("already exists");
                return bucket;
            }
            System.err.println(error.getMessage());
            throw e;
        }
    }

    public boolean fileExists(String fileName)
            throws IOException {
        try {
            Storage.Objects.Get getObject = storage.objects().get(containerName, fileName);
            return getObject.execute() != null;
        } catch (GoogleJsonResponseException e) {
            if (e.getDetails().getCode() == 404) {
                return false;
            }
            logger.debug("Error on file exists check on Google Storage", e);
        }
        return false;
    }

    public boolean uploadFile(String fileName, File file) {
        logger.debug("Start file upload [{}] to Google Storage [{}] container", fileName, containerName);
        return uploadFileWithRetry(fileName, file, 0);
    }

    private boolean uploadFileWithRetry(String fileName, File file, int count) {
        if (count == MAX_RETRY_COUNT) {
            logger.debug("Error on file download [{}]. All retries failed", fileName);
            return false;
        }
        try {
            StorageObject storageObject = uploadSimple(storage, containerName, fileName, new FileInputStream(file), getContentType(fileName));
//TODO return url https://storage.googleapis.com/artist-images/2a746f5d2d44e27df5f29d9bf013b920.jpg
            if (count != 0) {
                logger.debug("Successfully upload file [{}] with [{}] retry", fileName, count);
            }
            return true;
        } catch (GoogleJsonResponseException e) {
            logger.debug("API error on file upload on Google Storage.", e);
        } catch (IOException e) {
            logger.debug("IO Error on file upload check on Google Storage.", e);
        }
        count++;
        try {
            logger.debug("Waiting for retry for file [{}] attempts [{}]", fileName, count);
            Thread.sleep(500 * count);
        } catch (InterruptedException e) {
            logger.debug("Sleep error on file upload check on Google Storage.", e);
        }
        return uploadFileWithRetry(fileName, file, count);
    }

    private String getContentType(String fileName) {
        String lowerName = fileName.toLowerCase();
        if (lowerName.endsWith("png")) {
            return "image/png";
        }
        if (lowerName.endsWith("jpg") || lowerName.endsWith("jpeg")) {
            return "image/jpeg";
        }
        return "audio/mpeg";
    }

    private StorageObject uploadSimple(Storage storage, String bucketName, String objectName,
                                       InputStream data, String contentType) throws IOException {
        InputStreamContent mediaContent = new InputStreamContent(contentType, data);
        Storage.Objects.Insert insertObject = storage.objects().insert(bucketName, null, mediaContent)
                .setName(objectName);
        insertObject.setPredefinedAcl("publicread");
        // The media uploader gzips content by default, and alters the Content-Encoding accordingly.
        // GCS dutifully stores content as-uploaded. This line disables the media uploader behavior,
        // so the service stores exactly what is in the InputStream, without transformation.
        insertObject.getMediaHttpUploader().setDisableGZipContent(true);
        return insertObject.execute();
    }
}
