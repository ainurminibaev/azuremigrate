package me.musicsense.backblaze.storage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import synapticloop.b2.B2ApiClient;
import synapticloop.b2.BucketType;
import synapticloop.b2.exception.B2ApiException;
import synapticloop.b2.response.B2BucketResponse;
import synapticloop.b2.response.B2DownloadFileResponse;

/**
 * Created by ainurminibaev on 28.06.16.
 */
public class B2UploadTool {

    private final static Logger logger = LoggerFactory.getLogger(B2UploadTool.class);
    public static final int MAX_RETRY_COUNT = 3;


    private B2ApiClient b2ApiClient;

    private String containerName;
    private B2BucketResponse container;

    public B2UploadTool(String accountId, String token, String containerName) throws B2ApiException, IOException {
        this.b2ApiClient = new B2ApiClient();
        this.b2ApiClient.authenticate(accountId, token);
        this.containerName = containerName;
        List<B2BucketResponse> b2BucketResponses = b2ApiClient.listBuckets();
        for (B2BucketResponse b2BucketResponse : b2BucketResponses) {
            if (b2BucketResponse.getBucketName().equals(containerName)) {
                logger.debug("Container found on B2:" + containerName);
                container = b2BucketResponse;
            }
        }
        if (container == null) {
            logger.debug("Container does not exists on B2. Creating... " + containerName);
            container = b2ApiClient.createBucket(containerName, BucketType.allPublic);
        }
    }

    public boolean fileExists(String fileName) {
        B2DownloadFileResponse fileExists = null;
        try {
            fileExists = b2ApiClient.downloadFileRangeByName(containerName, fileName, 0, 1);
            return true;
        } catch (B2ApiException e) {
            if (e.getStatus() != 404) {
                logger.debug("API error on file exists check for B2.", e);
                return true;
            }
        } catch (IOException e) {
            logger.debug("Error on file exists check on B2.", e);
        } finally {
            if (fileExists != null && fileExists.getContent() != null) {
                try {
                    fileExists.getContent().close();
                } catch (IOException e) {
                    logger.debug("Error on stream close", e);
                }
            }
        }
        return false;
    }

    public boolean uploadFile(String fileName, File file) {
        logger.debug("Start file upload [{}] to [{}] container", fileName, containerName);
        return uploadFileWithRetry(fileName, file, 0);
    }

    private boolean uploadFileWithRetry(String fileName, File file, int count) {
        if (count == MAX_RETRY_COUNT) {
            logger.debug("Error on file download [{}]. All retries failed", fileName);
            return false;
        }
        try {
            b2ApiClient.uploadFile(container.getBucketId(), fileName, file);
            if (count != 0) {
                logger.debug("Successfully upload file [{}] with [{}] retry", fileName, count);
            }
            return true;
        } catch (B2ApiException e) {
            logger.debug("API error on file upload on B2.", e);
        } catch (IOException e) {
            logger.debug("IO Error on file upload check on B2.", e);
        }
        count++;
        try {
            logger.debug("Waiting for retry for file [{}] attempts [{}]", fileName, count);
            Thread.sleep(500 * count);
        } catch (InterruptedException e) {
            logger.debug("Sleep error on file upload check on B2.", e);
        }
        return uploadFileWithRetry(fileName, file, count);
    }
}
