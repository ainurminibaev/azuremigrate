package me.musicsense.backblaze;

import java.io.File;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.EnumSet;
import java.util.Iterator;

import com.blogspot.mydailyjava.guava.cache.overflow.FileSystemCacheBuilder;
import com.google.common.cache.Cache;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.ResultContinuation;
import com.microsoft.azure.storage.ResultSegment;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobListingDetails;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.ListBlobItem;
import me.musicsense.backblaze.azure.CopyOfResultContinuation;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ainurminibaev on 28.06.16.
 */
public class AzureFileReader {

    private final static Logger logger = LoggerFactory.getLogger(AzureFileReader.class);
    public static final int MAX_FILES_TO_GET = 2;

    private final CloudBlobContainer blobContainer;
    private final Integer pageSize;

    private ResultSegment<ListBlobItem> resultSegment;
    private Iterator<ListBlobItem> resultIteraror = null;
    private final Cache<String, CopyOfResultContinuation> cache;

    public AzureFileReader(String connString, String containerName, String cacheDir, Integer pageSize) throws URISyntaxException, InvalidKeyException, StorageException {
        CloudStorageAccount storageAccount = CloudStorageAccount.parse(connString);
        // Create the blob client
        CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
        this.blobContainer = blobClient.getContainerReference(containerName);
        File dir = new File(cacheDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        this.cache = FileSystemCacheBuilder.newBuilder()
                .persistenceDirectory(dir)
                .maximumSize(0L)
                .weakValues()
                .initialCapacity(0)
                .build();
        this.cache.put("stub", new CopyOfResultContinuation());
        this.pageSize = pageSize;
        logger.debug("Azure storage init: OK");
    }

    public synchronized ListBlobItem getFileToDownload() throws StorageException {
        if (resultSegment == null || !resultIteraror.hasNext() && resultSegment.getHasMoreResults()) {
            ResultContinuation continuationToken = resultSegment == null ? initFromCache() : resultSegment.getContinuationToken();
            if (continuationToken != null) {
                saveContinuationTokenToFile(continuationToken);
            }
            resultSegment = blobContainer.listBlobsSegmented(null, false, EnumSet.noneOf(BlobListingDetails.class), pageSize, continuationToken, null, null);
            resultIteraror = resultSegment.getResults().iterator();
        }
        if (resultIteraror.hasNext()) {
            return resultIteraror.next();
        }
        logger.debug("No files left");
        return null;
    }

    private ResultContinuation initFromCache() {
        CopyOfResultContinuation copyOfResultContinuation = cache.getIfPresent("token");
        if (copyOfResultContinuation == null) {
            return null;
        }
        ResultContinuation resultContinuation = new ResultContinuation();
        try {
            BeanUtils.copyProperties(resultContinuation, copyOfResultContinuation);
            return resultContinuation;
        } catch (Exception e) {
            logger.debug("Error on reading from cache", e);
        }
        return null;
    }

    private void saveContinuationTokenToFile(ResultContinuation continuationToken) {
        CopyOfResultContinuation copyOfResultContinuation = new CopyOfResultContinuation();
        try {
            BeanUtils.copyProperties(copyOfResultContinuation, continuationToken);
            cache.put("token", copyOfResultContinuation);
        } catch (Exception e) {
            logger.debug("Error on cache", e);
        }
    }


}
