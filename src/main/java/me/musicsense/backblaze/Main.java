package me.musicsense.backblaze;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Properties;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;
import me.musicsense.backblaze.storage.B2UploadTool;
import me.musicsense.backblaze.storage.GoogleStorageUploadTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import synapticloop.b2.exception.B2ApiException;

/**
 * Created by ainurminibaev on 28.06.16.
 */
public class Main {
    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static Properties properties;

    public static void readProperties() {
        try {
            properties = new Properties();
            String propFileName = "config.properties";
            InputStream inputStream = new FileInputStream(propFileName);
            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error on property read", e);
        }
    }


    public static void main(String[] args) throws URISyntaxException, InvalidKeyException, StorageException, IOException, B2ApiException {
        readProperties();

        String containerName = properties.getProperty("container.name");
        String azureConnStr = properties.getProperty("azure.connection");
        String cacheDir = properties.getProperty("cache.dir");
        String googleJsonCreds = properties.getProperty("google.creds.file");
        final GoogleStorageUploadTool googleStorageUploadTool = new GoogleStorageUploadTool(containerName, googleJsonCreds);
        final Boolean b2Enabled = Boolean.valueOf(properties.getProperty("b2.enabled"));
        Integer pageSize = Integer.valueOf(properties.getProperty("azure.page.size"));
        final AzureFileReader azureFileReader = new AzureFileReader(azureConnStr, containerName, cacheDir, pageSize);


        String b2AccountId = properties.getProperty("b2.account"); // your b2 account ID
        String b2ApplicationKey = properties.getProperty("b2.appkey"); // API key
        B2UploadTool b2UploadToolInit = null;
        if (b2Enabled) {
            b2UploadToolInit = new B2UploadTool(b2AccountId, b2ApplicationKey, containerName);
        }
//        FileUtils.writeStringToFile("lastUsedToken");

        // Get a reference to a container

        logger.debug("Start file uploader for thread Main.");

        final B2UploadTool b2UploadTool = b2UploadToolInit;
        Integer threadsCount = Integer.valueOf(properties.getProperty("threads.count"));
        for (int i = 0; i < threadsCount; i++) {
            final int finalI = i;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        logger.debug("Start work of download [in thread {}]", finalI);
                        do {
                            ListBlobItem fileToDownload = azureFileReader.getFileToDownload();
                            if (fileToDownload == null) {
                                break;
                            }
                            CloudBlockBlob blob = new CloudBlockBlob(fileToDownload.getUri());
                            String fileName = blob.getName();
                            boolean b2NeedToDownload = false;
                            boolean googleNeedToDownload = false;
                            if (b2Enabled && !b2UploadTool.fileExists(fileName)) {
                                b2NeedToDownload = true;
                            }
                            if (!googleStorageUploadTool.fileExists(fileName)) {
                                googleNeedToDownload = true;
                            }
                            if (!b2NeedToDownload && !googleNeedToDownload) {
                                continue;
                            }
                            logger.debug("Download file [{}] to local memory [in thread {}]", fileName, finalI);
                            blob.downloadToFile(fileName);
                            logger.debug("End of download file [{}] to local memory [in thread {}]", fileName, finalI);
                            File localFile = new File(fileName);
                            if (b2NeedToDownload) {
                                b2UploadTool.uploadFile(fileName, localFile);
                            }
                            if (googleNeedToDownload) {
                                googleStorageUploadTool.uploadFile(fileName, localFile);
                            }
                            localFile.delete();
                        } while (true);
                    } catch (Exception e) {
                        logger.error("Error on process for Thread " + finalI, e);
                    }
                }
            }).start();
        }
    }
}
