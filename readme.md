### Run this with near config.properties and client_secrets.json file
java -Dorg.slf4j.simpleLogger.defaultLogLevel=debug -jar target/azure-to-backblaze-1.0-SNAPSHOT-jar-with-dependencies.jar

# 1 client_secrets.json Get it from google developer console. Enable Google Storage API
# 2 config.properties application properties
# 3 build it with
mvn clean compile assembly:single

# 4 For build with Maven you need to add lib/backblaze-b2-java-api-1.3.4.jar to local mvn repository

# 5 If all of code is suitable just use jar from build folder